package 多线程;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Callable详细用法可参考: https://dolphin0520.cnblogs.com/p/3949310.html?from=timeline
 * Created by huangyu on 2018/8/30 21:18
 */
public class CallableDemo {

	/**
	 * Callable与run方法最大的区别在于线程执行完毕有返回值
	 */
	static class MyCallable implements Callable<String> {
		@Override
		public String call() throws Exception {
			System.out.println("子线程正在执行");
			Thread.sleep(3000);
			return "我是执行结果";
		}
	}

	public static void main(String[] args) throws Exception {
		ExecutorService executorService = Executors.newCachedThreadPool();
		Future<String> future = executorService.submit(new MyCallable());
		String result = future.get();// get方法获取执行结果, 该方法会阻塞直到任务返回结果.
		System.out.println("主线程正在执行");
		System.out.println(result);

	}
}
