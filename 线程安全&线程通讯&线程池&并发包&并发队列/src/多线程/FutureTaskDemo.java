package 多线程;

import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

/**
 * FutureTask详细用法可参考: https://dolphin0520.cnblogs.com/p/3949310.html?from=timeline
 * Created by huangyu on 2018/8/30 22:10
 */
public class FutureTaskDemo {

	public static void main(String[] args) throws Exception {
		FutureTask<String> futureTask = new FutureTask<String>(new Callable<String>() {
			@Override
			public String call() throws Exception {
				System.out.println("子线程正在执行");
				Thread.sleep(5000);
				return "我是执行结果";
			}
		});

		new Thread(futureTask).start();
		System.out.println("主线程正在执行");
		Thread.sleep(1000);
		String result = futureTask.get();
		System.out.println(result);
	}
}
