package 并发队列.非阻塞队列;

import java.util.concurrent.ConcurrentLinkedDeque;

/**
 * Created by huangyu on 2018/9/1 16:21
 */
public class ConcurrentLinkedDequeDemo {

	/**
	 * ConcurrentLinkedQueue ：是一个适用于高并发场景下的队列，通过无锁的方式，实现
	 * 了高并发状态下的高性能，通常ConcurrentLinkedQueue性能好于BlockingQueue.它
	 * 是一个基于链接节点的无界线程安全队列。该队列的元素遵循先进先出的原则。头是最先
	 * 加入的，尾是最近加入的，该队列不允许null元素。
	 */
	public static void main(String[] args) {
		ConcurrentLinkedDeque deque = new ConcurrentLinkedDeque();
		/*add 和offer() 都是加入元素的方法(在ConcurrentLinkedQueue中这俩个方法没有任何区别)
			poll() 和peek() 都是取头元素节点，区别在于前者会删除元素，后者不会。*/
		deque.offer("张三");
		deque.offer("李四");

		Object peek = deque.peek();

	}

}
