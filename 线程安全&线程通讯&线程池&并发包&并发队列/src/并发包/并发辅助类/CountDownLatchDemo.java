package 并发包.并发辅助类;

import java.util.concurrent.CountDownLatch;

/**
 * 【主题】CountDownLatch
 * <p>
 * 详细用法可参考: http://www.importnew.com/21889.html?from=timeline
 * 在java 1.5中，提供了一些非常有用的辅助类来帮助我们进行并发编程,
 * 比如CountDownLatch、CyclicBarrier、Semaphore, 今天我们就来学习一下这三个辅助类的用法。
 * </p>
 * Created by huangyu on 2018/9/1 13:32
 */
public class CountDownLatchDemo {

	/**
	 * 【常用方法】
	 * 1.CountDownLatch类位于java.util.concurrent包下，利用它可以实现类似计数器的功能。
	 * 比如有一个任务A，它要等待其他4个任务执行完毕之后才能执行，此时就可以利用CountDownLatch来实现这种功能了。
	 *
	 * 2.CountDownLatch类只提供了一个构造器：
	 * public CountDownLatch(int count) {}; //参数count为计数值
	 *
	 * 3.这3个方法是CountDownLatch类中最重要的方法：
	 * public void await() throws InterruptedException { }; //调用await()方法的线程会被挂起，它会等待直到count值为0才继续执行
	 * public boolean await(long timeout, TimeUnit unit) throws InterruptedException { }; //和await()类似，只不过等待一定的时间后count值还没变为0的话就会继续执行
	 * public void countDown() { }; //将count值减1
	 */


	/**
	 * CountDownLatch一般用于某个线程A等待若干个其他线程执行完任务之后，它才执行
	 */
	public static void main(String[] args) {
		final CountDownLatch latch = new CountDownLatch(2);
		new Thread() {
			public void run() {
				try {
					System.out.println("子线程" + Thread.currentThread().getName() + "正在执行");
					Thread.sleep(3000);
					System.out.println("子线程" + Thread.currentThread().getName() + "执行完毕");
					latch.countDown();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

			;
		}.start();

		new Thread() {
			public void run() {
				try {
					System.out.println("子线程" + Thread.currentThread().getName() + "正在执行");
					Thread.sleep(3000);
					System.out.println("子线程" + Thread.currentThread().getName() + "执行完毕");
					latch.countDown();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

			;
		}.start();

		try {

			latch.await();
			System.out.println("2个子线程已经执行完毕");
			System.out.println("继续执行主线程");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
